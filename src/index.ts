import express from 'express';
import { join } from './actions/join';
import { startGame } from './actions/start';
import { start } from './commands/start';
import { callbackMap } from './messaging/responses';
import { buildAssignRoleMessage } from './messaging';
import { ActionResponse } from './api';
import { quit } from './commands/quit';
import { remind } from './commands/remind';
import { config } from 'dotenv';

if (process.env.NODE_ENV == 'dev') {
  console.log('loading .env file');
  config();
}

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', (_, res) => res.send('okay'));

app.post('/command', async (req, res) => {
  const args = req.body.text.split(' ');
  switch (args[0]) {
    case 'start':
      res.json(await start(req.body, args[1]));
      break;
    case 'quit':
      res.json(await quit(req.body));
      break;
    case 'remind':
      res.json(await remind(req.body));
      break;
    default:
      const role = req.body.text.split(' ')[1];
      res.json(
        buildAssignRoleMessage(
          role,
          req.body.user_id,
          [req.body.user_id, 'test', 'test', 'test'],
          req.body.user_id,
        ),
      );
      return;
  }
});

app.post('/action', (req, res) => {
  const body = JSON.parse(req.body.payload);

  body.actions.forEach(
    ({ action_id, value, selected_option }: ActionResponse) => {
      const callback = callbackMap.get(action_id);
      if (callback) {
        callback({
          response: selected_option ? selected_option.value : value,
          response_url: body.response_url,
        });
      }
    },
  );

  const {
    actions: [{ value }],
  } = body;

  switch (value) {
    case 'join':
      join(body);
      break;

    case 'start-game':
      startGame(body);
      break;
  }

  res.status(200).send();
});

app.listen(3000, () => {
  console.log('listening');
});
