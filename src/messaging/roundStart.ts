export const buildRoundStartMessage = (president: string) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `*${president}* is president.`,
    },
  },
];
