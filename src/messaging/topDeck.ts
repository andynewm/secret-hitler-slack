import { Membership } from '../game/secretHitler';

export const buildTopDeckMessage = (
  policy: Membership,
  leftInDraw: number,
  shuffled: boolean,
) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `A ${policy} policy was flipped from the top of the deck`,
    },
    accessory: {
      type: 'image',
      image_url: `https://secret-hitler-slack-assets.s3.eu-west-2.amazonaws.com/${policy.slice(
        0,
        3,
      )}-pol-sq.png`,
      alt_text: policy,
    },
  },
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `${
        shuffled ? '*Shuffled policies*! ' : ''
      }There are ${leftInDraw} policies ${
        shuffled ? 'now' : 'left'
      } in the draw pile.`,
    },
  },
];
