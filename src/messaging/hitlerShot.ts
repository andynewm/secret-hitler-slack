import { assets } from '../assets';

export const buildHitlerShotMessage = (hitler: string) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `*${hitler}* was shot! Liberals win.`,
    },
    accessory: {
      type: 'image',
      image_url: assets.role.hitler,
      alt_text: 'liberal',
    },
  },
];
