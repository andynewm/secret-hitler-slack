import request from 'request';

export * from './board';
export * from './chooseChancellor';
export * from './choosePresident';
export * from './discard';
export * from './draw';
export * from './gameEnd';
export * from './hitlerElected';
export * from './hitlerShot';
export * from './investigation';
export * from './notHitler';
export * from './peek';
export * from './playerShot';
export * from './policiesEnd';
export * from './responses';
export * from './roleAssign';
export * from './roundStart';
export * from './shoot';
export * from './startGame';
export * from './topDeck';
export * from './vote';
export * from './voteOutcome';

export const sendMessage = (channel: string, message: any) => {
  const options = {
    method: 'POST',
    url: 'https://slack.com/api/chat.postMessage',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${process.env.SLACK_TOKEN}`,
    },
    body: {
      channel,
      blocks: message,
    },
    json: true,
  };

  request(options, (err, response, body) => {
    if (err) {
      console.log('ERROR', err);
    } else {
      console.log('response', body);
    }
  });
};

export const sendEphemeral = (channel: string, user: string, message: any) => {
  const options = {
    method: 'POST',
    url: 'https://slack.com/api/chat.postEphemeral',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${process.env.SLACK_TOKEN}`,
    },
    body: {
      channel,
      user,
      blocks: message,
    },
    json: true,
  };

  request(options, (err, response, body) => {
    if (err) {
      console.log('ERROR', err);
    } else {
      console.log('response', body);
    }
  });
};
