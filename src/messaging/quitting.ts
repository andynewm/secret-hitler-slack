export const quitting = (quitter: string) => {
  return [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `${quitter} quit the game of Secret Hitler!`,
      },
    },
  ];
};
