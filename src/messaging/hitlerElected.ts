import { assets } from '../assets';

export const buildHitlerElectedMessage = (hitler: string) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `*${hitler}* was elected as chancellor! Fascists win.`,
    },
    accessory: {
      type: 'image',
      image_url: assets.role.hitler,
      alt_text: 'liberal',
    },
  },
];
