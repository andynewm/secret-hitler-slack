import { assets } from '../assets';
import { Membership } from '../game/secretHitler';
import { toTitleCase } from '../utils';

export const buildBoardMessage = (
  played: Membership,
  playerCount: number,
  liberalPolicies: number,
  fascistPolicies: number,
  president: string,
  chancellor: string,
  leftInDraw: number,
  shuffled: boolean,
) => {
  const fascistImage = assets.board.fascist(playerCount, fascistPolicies);
  const liberalImage = assets.board.liberal(liberalPolicies);

  return [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `${toTitleCase(played)} policy played`,
      },
      accessory: {
        type: 'image',
        image_url: `https://secret-hitler-slack-assets.s3.eu-west-2.amazonaws.com/${played.slice(
          0,
          3,
        )}-pol-sq.png`,
        alt_text: played,
      },
    },
    {
      type: 'divider',
    },
    {
      type: 'image',
      title: {
        type: 'plain_text',
        text: 'liberal',
      },
      image_url: liberalImage,
      alt_text: 'liberal',
    },
    {
      type: 'image',
      title: {
        type: 'plain_text',
        text: 'fascist',
      },
      image_url: fascistImage,
      alt_text: 'fascist',
    },
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `Last government was: President: *${president}*, Chancellor: *${chancellor}*.`,
      },
    },
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `${shuffled ? '*Shuffled policies*! ' : ''
          }There are ${leftInDraw} policies ${shuffled ? 'now' : 'left'
          } in the draw pile.`,
      },
    },
  ];
};
