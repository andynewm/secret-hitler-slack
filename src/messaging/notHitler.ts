export const buildNotHitlerMessage = (notHitler: string) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `*${notHitler}* is not Hitler! (Totally _might_ still be a fascist though.)`,
    },
  },
];
