import { Player } from '../game/game';
import { assets } from '../assets';
import { Membership } from '../game/secretHitler';

export const buildInvestigationStartingMessage = (president: string) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `*${president}* is choosing whose card to look at`,
    },
  },
];

export const buildInvestigationMessage = (candidates: Player[]) => (
  action_id: string,
) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: 'Choose whose membership can to look at',
    },
    accessory: {
      type: 'static_select',
      placeholder: {
        type: 'plain_text',
        text: 'Player',
      },
      action_id,
      confirm: {
        title: {
          type: 'plain_text',
          text: 'Are you sure?',
        },
        text: {
          type: 'mrkdwn',
          text: 'Is that your choice?',
        },
        confirm: {
          type: 'plain_text',
          text: 'Yes',
        },
        deny: {
          type: 'plain_text',
          text: 'No',
        },
      },
      options: candidates.map(({ name, userId }) => ({
        text: {
          type: 'plain_text',
          text: name,
        },
        value: userId,
      })),
    },
  },
];

export const buildInvestigationUpdate = (chancellor: string) => ({
  response_type: 'ephemeral',
  replace_original: true,
  blocks: [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `You chose *${chancellor}* to look at`,
      },
    },
  ],
});

export const buildInvestigationResultMessage = (
  investigated: string,
  role: Membership,
) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `You see that *${investigated}*'s membership card is *${role}*`,
    },
    accessory: {
      type: 'image',
      image_url: assets.membership[role],
      alt_text: role,
    },
  },
];

export const buildInvestigationPublicMessage = (
  president: string,
  investigated: string,
) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `*${president}* chose to look at *${investigated}*'s membership card.`,
    },
  },
];
