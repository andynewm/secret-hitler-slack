export const buildPresidentDiscardedMessage = (president: string, chancellor: string) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `${president} has discarded a card and handed two to ${chancellor}`,
    },
  },
];
