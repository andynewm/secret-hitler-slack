import { Membership } from '../game/secretHitler';
import { assets } from '../assets';

export const buildPeekNotificationMessage = (president: string) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `${president} is taking a look at the top three cards`,
    },
  },
];

export const buildPeekMessage = (deck: Membership[]) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: 'The top three policy card are',
    },
  },
  {
    type: 'context',
    elements: [
      {
        type: 'mrkdwn',
        text: 'Next card to be played on the right:',
      },
      ...deck.map(policy => ({
        type: 'image',
        image_url: assets.policy[policy],
        alt_text: policy,
      })),
    ],
  },
];
