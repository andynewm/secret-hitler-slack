export const buildGameEndMessage = (
  liberals: string[],
  fascists: string[],
  hitler: string,
) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `:liberal-member:*Liberals:* ${liberals.join(', ')}`,
    },
  },
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `:fascist-member:*Fascists:* ${fascists.join(', ')}`,
    },
  },
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `:hitler:*Hitler:* ${hitler}`,
    },
  },
];
