import { Role } from '../game/secretHitler';
import { Player } from '../game/game';

export const buildAssignRoleMessage = (
  role: Role,
  player: Player,
  fascists: Player[],
  hitler: Player,
) => {
  switch (role) {
    case 'liberal':
      return [
        {
          type: 'section',
          text: {
            type: 'mrkdwn',
            text: 'Your role is *liberal*',
          },
          accessory: {
            type: 'image',
            image_url:
              'https://secret-hitler-slack-assets.s3.eu-west-2.amazonaws.com/lib-role.png',
            alt_text: 'liberal',
          },
        },
      ];

    case 'fascist':
      return [
        {
          type: 'section',
          text: {
            type: 'mrkdwn',
            text: 'Your role is *fascist*',
          },
          accessory: {
            type: 'image',
            image_url:
              'https://secret-hitler-slack-assets.s3.eu-west-2.amazonaws.com/fas-role.png',
            alt_text: 'fascist',
          },
        },
        {
          type: 'divider',
        },
        {
          type: 'section',
          text: {
            type: 'mrkdwn',
            text: `The other fascist${
              fascists.length === 2 ? ' is' : 's are'
            } ${fascists
              .filter(x => x.userId !== player.userId)
              .map(x => `*${x.name}*`)
              .join(', ')}`,
          },
        },
        {
          type: 'section',
          text: {
            type: 'mrkdwn',
            text: `Hitler is *${hitler.name}*`,
          },
        },
      ];

    case 'hitler':
      return [
        {
          type: 'section',
          text: {
            type: 'mrkdwn',
            text: 'Your role is *Hitler*',
          },
          accessory: {
            type: 'image',
            image_url:
              'https://secret-hitler-slack-assets.s3.eu-west-2.amazonaws.com/hit-role.png',
            alt_text: 'hitler',
          },
        },
        ...(fascists.length === 1
          ? [
              {
                type: 'divider',
              },
              {
                type: 'section',
                text: {
                  type: 'mrkdwn',
                  text: `The other fascist is *${fascists[0].name}*`,
                },
              },
            ]
          : []),
      ];
  }
};
