import { Membership } from '../game/secretHitler';
import { assets } from '../assets';

export const buildDrawMessage = (policies: Membership[], canVeto: boolean) => (
  firstPolicyAction: string,
  secondPolicyAction: string,
  thirdPolicyAction: string,
  vetoAction: string,
) => [
  {
    type: 'context',
    elements: [
      {
        type: 'mrkdwn',
        text: `You ${policies.length === 3 ? 'picked up' : 'were passed'}:`,
      },
      ...policies.map(policy => ({
        type: 'image',
        image_url: assets.policy[policy],
        alt_text: policy,
      })),
    ],
  },
  {
    type: 'actions',
    elements: [
      ...policies.map((policy, i) => ({
        action_id: [firstPolicyAction, secondPolicyAction, thirdPolicyAction][
          i
        ],
        type: 'button',
        text: {
          type: 'plain_text',
          text: `Discard ${policy}`,
        },
        value: i.toString(),
        style: policy === 'fascist' ? 'danger' : 'primary',
      })),
      ...(canVeto
        ? [
            {
              action_id: vetoAction,
              type: 'button',
              text: {
                type: 'plain_text',
                text: 'Veto',
              },
              value: 'veto',
            },
          ]
        : []),
    ],
  },
];

export const buildDrawUpdate = (policies: Membership[]) => (
  discarded: string,
) => ({
  response_type: 'ephemeral',
  replace_original: true,
  blocks: [
    {
      type: 'context',
      elements: [
        {
          type: 'mrkdwn',
          text: `You ${policies.length === 3 ? 'picked up' : 'were passed'}:`,
        },
        ...policies.map(policy => ({
          type: 'image',
          image_url: assets.policy[policy],
          alt_text: policy,
        })),
      ],
    },
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text:
          discarded === 'veto'
            ? 'Vetoed'
            : `Discarded ${policies[parseInt(discarded)]} policy`,
      },
    },
  ],
});
