export const startGame = (players: string[]) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: 'A new game of Secret Hitler begins! Play order is:'
    }
  },
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: players.join(', ')
    }
  }
];
