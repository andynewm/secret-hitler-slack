import { Game } from '../game/game';

export const starting = (game: Game) => {
  const originator = game.players[0];

  return [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `${originator.name} is starting a game of Secret Hitler!`,
      },
      accessory: {
        type: 'button',
        text: {
          type: 'plain_text',
          text: 'Join',
        },
        value: 'join',
      },
    },
    {
      type: 'divider',
    },
    ...(game.players.length > 1
      ? [
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: `Playing: ${game.players
                .map(({ name }) => name)
                .join(', ')}`,
            },
          },
        ]
      : []),
    ...(game.players.length < 5
      ? [
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: `Only need ${5 -
                game.players.length} more players to start.`,
            },
          },
        ]
      : []),
    ...(game.players.length >= 5
      ? [
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: 'We are ready to go',
            },
            accessory: {
              type: 'button',
              text: {
                type: 'plain_text',
                text: 'Start Game',
              },
              value: 'start-game',
            },
          },
        ]
      : []),
  ];
};
