import request from 'request';
import uuid from 'uuid/v4';
import { sendEphemeral } from '.';

export const callbackMap = new Map();

const resendMap = new Map<string, string>();

const messagePlayer = (response_url: string, message: any) => {
  request({
    uri: response_url,
    method: 'POST',
    json: message,
  });
};

export const getLastMessage = (channel: string, user: string) => {
  const resendKey = `${channel}.${user}`;
  const message = resendMap.get(resendKey);

  return message;
};

export const getResponse = async (
  channel: string,
  user: string,
  fn: (...actionIds: string[]) => any,
  update: any,
) => {
  const actionIds = [...Array(fn.length)].map(() => uuid());
  const message = fn(...actionIds);

  const resendKey = `${channel}.${user}`;

  resendMap.set(resendKey, message);

  sendEphemeral(channel, user, message);

  const { response, response_url } = await Promise.race<any>(
    actionIds.map(getPromise),
  );

  resendMap.delete(resendKey);

  if (update) {
    const rr = update(response);
    messagePlayer(response_url, rr);
  }

  return response;
};

const getPromise = (actionId: string) =>
  new Promise((resolve) => {
    callbackMap.set(actionId, resolve);
  });
