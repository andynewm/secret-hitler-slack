import { Player } from "../game/game";

export const buildVoteMessage = (president: string, chancellor: string, players: string[]) => (
  jaAction: string,
  neinAction: string
) => [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `*${president}* has chosen *${chancellor}* to be their chancellor. Do you approve?`
      }
    },
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: 'Play order is: ' + players.join(', ')
      }
    },
    {
      type: 'actions',
      elements: [
        {
          action_id: jaAction,
          type: 'button',
          text: {
            type: 'plain_text',
            text: 'Ja'
          },
          value: 'ja',
          style: 'primary'
        },
        {
          action_id: neinAction,
          type: 'button',
          text: {
            type: 'plain_text',
            text: 'Nein'
          },
          value: 'nein',
          style: 'danger'
        }
      ]
    }
  ];

export const buildVoteUpdate = (president: string, chancellor: string) => (
  vote: string
) => ({
  response_type: 'ephemeral',
  replace_original: true,
  blocks: [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `*${president}* has chosen *${chancellor}* to be their chancellor. Do you approve?`
      }
    },
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `You voted *${vote}*.`
      }
    }
  ]
});

export const buildVoteWaitingMessage = (
  slowPlayers: Player[]
) => [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `Waiting for ${slowPlayers.map(player => player.name).join(', ')} to vote. If they do not vote in the next 30 seconds, they will automatically be *nein*.`,
      },
    }
  ];

