import { Membership } from '../game/secretHitler';
import { toTitleCase } from '../utils';

export const buildPoliciesEndMessage = (winner: Membership) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `${winner === 'fascist' ? 6 : 5} ${toTitleCase(
        winner,
      )} policies played, *${toTitleCase(winner)}s* win!`,
    },
    accessory: {
      type: 'image',
      image_url: `https://secret-hitler-slack-assets.s3.eu-west-2.amazonaws.com/${winner.slice(
        0,
        3,
      )}-role.png`,
      alt_text: 'winner',
    },
  },
];
