export const buildVoteOutcomeMessage = (
  votePassed: boolean,
  votes: { user: string; vote: string }[],
) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `Vote *${votePassed ? 'passed' : 'failed'}*`,
    },
  },
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: votes.map(({ user, vote }) => `*${user}*: ${vote}`).join(' '),
    },
  },
];
