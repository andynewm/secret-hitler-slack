import { Player } from '../game/game';

export const buildChoosePresidentMessage = (candidates: Player[], players: string[]) => (
  action_id: string,
) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: 'Choose your next president',
    },
    accessory: {
      type: 'static_select',
      placeholder: {
        type: 'plain_text',
        text: 'President',
      },
      action_id,
      confirm: {
        title: {
          type: 'plain_text',
          text: 'Are you sure?',
        },
        text: {
          type: 'mrkdwn',
          text: 'Is that your choice?',
        },
        confirm: {
          type: 'plain_text',
          text: 'Yes',
        },
        deny: {
          type: 'plain_text',
          text: 'No',
        },
      },
      options: candidates.map(({ name, userId }) => ({
        text: {
          type: 'plain_text',
          text: name,
        },
        value: userId,
      })),
    },
  },
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: 'Play order is: ' + players.join(', ')
    }
  },
];

export const buildChoosePresidentUpdate = (president: string) => ({
  response_type: 'ephemeral',
  replace_original: true,
  blocks: [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `You chose *${president}* to be your president.`,
      },
    },
  ],
});
