import { Player } from '../game/game';

export const buildChooseChancellorMessage = (candidates: Player[], players: string[]) => (
  action_id: string,
) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: 'Choose your chancellor',
    },
    accessory: {
      type: 'static_select',
      placeholder: {
        type: 'plain_text',
        text: 'Chancellor',
      },
      action_id,
      confirm: {
        title: {
          type: 'plain_text',
          text: 'Are you sure?',
        },
        text: {
          type: 'mrkdwn',
          text: 'Is that your choice?',
        },
        confirm: {
          type: 'plain_text',
          text: 'Yes',
        },
        deny: {
          type: 'plain_text',
          text: 'No',
        },
      },
      options: candidates.map(({ name }) => ({
        text: {
          type: 'plain_text',
          text: name,
        },
        value: name,
      })),
    },
  },
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: 'Play order is: ' + players.join(', ')
    }
  },
];

export const buildChooseChancellorUpdate = (chancellor: string) => ({
  response_type: 'ephemeral',
  replace_original: true,
  blocks: [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `You chose *${chancellor}* to be your chancellor.`,
      },
    },
  ],
});
