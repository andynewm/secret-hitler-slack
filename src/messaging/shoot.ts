import { Player } from '../game/game';

export const buildShootMessage = (candidates: Player[]) => (
  action_id: string,
) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: 'Choose who to shoot',
    },
    accessory: {
      type: 'static_select',
      placeholder: {
        type: 'plain_text',
        text: 'Victim',
      },
      action_id,
      confirm: {
        title: {
          type: 'plain_text',
          text: 'Are you sure?',
        },
        text: {
          type: 'mrkdwn',
          text: 'Is that your choice?',
        },
        confirm: {
          type: 'plain_text',
          text: 'Yes',
        },
        deny: {
          type: 'plain_text',
          text: 'No',
        },
      },
      options: candidates.map(({ name, userId }) => ({
        text: {
          type: 'plain_text',
          text: name,
        },
        value: userId,
      })),
    },
  },
];

export const buildShootUpdate = (victim: string) => ({
  response_type: 'ephemeral',
  replace_original: true,
  blocks: [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `You shot *${victim}*.`,
      },
    },
  ],
});
