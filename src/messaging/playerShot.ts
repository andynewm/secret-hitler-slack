export const buildPlayerShotMessage = (victim: string) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `*${victim}* was shot!`
    }
  }
];
