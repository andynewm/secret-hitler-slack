import request from 'request';
import { activeGames } from '../state';
import { starting } from '../messaging/starting';
import { ActionResponse, getName } from '../api';

export const join = async ({
  user: { id: userId },
  channel: { id: channelId },
  response_url,
}: ActionResponse) => {
  const game = activeGames.get(channelId);

  const name = await getName(userId);

  if (game) {
    game.join(userId, name);
    request({
      uri: response_url,
      method: 'POST',
      json: {
        response_type: 'in_channel',
        replace_original: true,
        blocks: starting(game),
      },
    });
  } else {
    request({
      uri: response_url,
      method: 'POST',
      json: {
        text: 'Error, game does not exist.',
        response_type: 'ephemeral',
      },
    });
  }
};
