import { activeGames } from '../state';
import { ActionResponse } from '../api';

export const startGame = ({ channel: { id: channelId } }: ActionResponse) => {
  const game = activeGames.get(channelId);
  if (game) {
    game.start();
  }
};
