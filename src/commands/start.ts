import { activeGames } from '../state';
import { Game } from '../game/game';
import { starting } from '../messaging/starting';
import { SlashResponse, getName } from '../api';

const wrapMessage = (message: string) => ({
  response_type: 'ephemeral',
  blocks: [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: message,
      },
    },
  ],
});

export const start = async (
  { channel_id, channel_name, user_id }: SlashResponse,
  bots: number,
) => {
  const game = activeGames.get(channel_id);
  const name = await getName(user_id);
  if (game) {
    return wrapMessage(joinGame(game, user_id, name));
  } else {
    return createGame(channel_id, channel_name, user_id, name, bots);
  }
};

const joinGame = (game: Game, userId: string, name: string) => {
  const joined = game.join(userId, name);
  return joined == null
    ? `Welcome to the game ${name}!`
    : joined == 'ingame'
    ? `You are already in the game ${name}!`
    : joined == 'started'
    ? `Sorry, the game has already begun`
    : '';
};

const createGame = (
  channel_id: string,
  channel_name: string,
  user_id: string,
  user_name: string,
  bots: number,
) => {
  const game = new Game(channel_id, channel_name);
  game.join(user_id, user_name);

  for (let i = 0; i < bots; i++) {
    game.join('bot' + i, 'bot' + i, true);
  }

  activeGames.set(channel_id, game);
  return {
    response_type: 'in_channel',
    blocks: starting(game),
  };
};
