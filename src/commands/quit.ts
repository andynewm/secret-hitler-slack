import { activeGames } from '../state';
import { quitting } from '../messaging/quitting';
import { SlashResponse, getName } from '../api';

const wrapMessage = (message: string) => ({
  response_type: 'ephemeral',
  blocks: [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: message,
      },
    },
  ],
});

export const quit = async ({ channel_id, user_id }: SlashResponse) => {
  const game = activeGames.get(channel_id);
  const name = await getName(user_id);
  if (game) {
    return quitGame(channel_id, name);
  } else {
    return wrapMessage('There is no active game to quit...');
  }
};

const quitGame = (channel_id: string, user_name: string) => {
  activeGames.delete(channel_id);

  return {
    response_type: 'in_channel',
    blocks: quitting(user_name),
  };
};
