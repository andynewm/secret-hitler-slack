import { activeGames } from '../state';
import { quitting } from '../messaging/quitting';
import { SlashResponse, getName } from '../api';
import { getLastMessage } from '../messaging/responses';

const wrapMessage = (message: string) => ({
  response_type: 'ephemeral',
  blocks: [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: message,
      },
    },
  ],
});

export const remind = async ({ channel_id, user_id }: SlashResponse) => {
  const game = activeGames.get(channel_id);
  const name = await getName(user_id);

  return {
    response_type: 'ephemeral', blocks: getLastMessage(channel_id, user_id)
  } ?? wrapMessage('...nothing here');
};

const quitGame = (channel_id: string, user_name: string) => {
  activeGames.delete(channel_id);

  return {
    response_type: 'in_channel',
    blocks: quitting(user_name),
  };
};
