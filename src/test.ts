import { playSecretHitler } from './game/secretHitler';

playSecretHitler(10, {
  assignRoles(roles) {
    console.log('assign: ' + roles);
  },
  cardPlayed(...args) {
    console.log('cardPlayed', args);
  },
  choosePresident(...args) {
    console.log('choosePresident', args);
    return Promise.resolve(args[1][0]);
  },
  fascistPolicies(...args) {
    console.log('fascistPolicies', args);
  },
  liberalPolicies(...args) {
    console.log('liberalPolicies', args);
  },
  hitlerShot(...args) {
    console.log('hitlerShot', args);
  },
  hitlerElected(...args) {
    console.log('hitlerElected', args);
  },
  viewInvestigation(...args) {
    console.log('viewInvestigation', args);
  },
  investigate(...args) {
    console.log('investigate', args);
    return Promise.resolve(args[1][0]);
  },
  selectChancellor(...args) {
    console.log('selectChancellor', args);
    return Promise.resolve(args[1][0]);
  },
  playPolicy(...args) {
    console.log('playPolicy', args);
    return Promise.resolve(0);
  },
  peekDeck(...args) {
    console.log('peekDeck', args);
  },
  vote(...args) {
    console.log('vote', args);
    return Promise.resolve(true);
  },
  topDeck(...args) {
    console.log('topDeck', args);
  },
  playerShot(...args) {
    console.log('playerShot', args);
  },
  shoot(...args) {
    console.log('shoot', args);
    return Promise.resolve(args[1][0]);
  },
  roundStart(...args) {
    console.log('roundStart', args);
  },
});
