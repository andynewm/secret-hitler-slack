const base = 'https://secret-hitler-slack-assets.s3.eu-west-2.amazonaws.com';

const asset = (name: string) => `${base}/${name}.png`;

const getBoard = (playerCount: number) => {
  if (playerCount <= 6) return '56';
  if (playerCount <= 8) return '78';
  return '910';
};

export const assets = {
  role: {
    liberal: asset('lib-role'),
    fascist: asset('fas-role'),
    hitler: asset('hit-role'),
  },
  board: {
    liberal: (policies: number) => asset(`lib-brd-${policies}`),
    fascist: (playerCount: number, policies: number) =>
      asset(`fas-brd-${getBoard(playerCount)}-${policies}`),
  },
  policy: {
    liberal: asset('lib-pol-sq'),
    fascist: asset('fas-pol-sq'),
  },
  membership: {
    liberal: asset('lib-mem'),
    fascist: asset('fas-mem'),
  },
};
