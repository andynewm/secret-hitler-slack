export function shuffle<T>(array: T[]) {
  const shuffled = Array.from(array);
  let m = shuffled.length;

  while (m) {
    const i = Math.floor(Math.random() * m--);
    const t = shuffled[m];
    shuffled[m] = shuffled[i];
    shuffled[i] = t;
  }

  return shuffled;
}

export const delay = (ms: number) => new Promise(res => setTimeout(res, ms));

export const toTitleCase = (str: string) => str[0].toUpperCase() + str.slice(1);
