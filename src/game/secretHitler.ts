import { shuffle } from '../utils';

export type Role = 'liberal' | 'fascist' | 'hitler';
export type Membership = 'liberal' | 'fascist';

const getRoles = (playerCount: number) =>
  shuffle(
    ([
      'hitler',
      'fascist',
      'liberal',
      'liberal',
      'liberal',
      'liberal',
      'fascist',
      'liberal',
      'liberal',
      'fascist',
      'liberal',
    ] as Role[]).slice(0, playerCount),
  );

const getDeck = () =>
  shuffle([
    'liberal',
    'liberal',
    'liberal',
    'liberal',
    'liberal',
    'liberal',
    'fascist',
    'fascist',
    'fascist',
    'fascist',
    'fascist',
    'fascist',
    'fascist',
    'fascist',
    'fascist',
    'fascist',
    'fascist',
  ] as Membership[]);

const getPresidentialPowers = (playerCount: number) => {
  switch (playerCount) {
    case 5:
    case 6:
      return [null, null, 'investigate', 'shoot', 'shoot'];
    case 7:
    case 8:
      return [null, 'investigate', 'choosePresident', 'shoot', 'shoot'];
    case 9:
    case 10:
      return [
        'investigate',
        'investigate',
        'choosePresident',
        'shoot',
        'shoot',
      ];
    default:
      throw Error(`No known presedential powers for ${playerCount} players`);
  }
};

export interface SecretHitlerCallbacks {
  assignRoles: (roles: Role[]) => Promise<void>;
  vote: (
    president: number,
    chancellor: number,
    livePlayers: number[],
  ) => Promise<boolean>;
  selectChancellor: (
    president: number,
    eligiblePlayers: number[],
  ) => Promise<number>;
  shoot: (president: number, eligiblePlayers: number[]) => Promise<number>;
  peekDeck: (president: number, top: Membership[]) => Promise<void>;
  investigate: (
    president: number,
    eligiblePlayers: number[],
  ) => Promise<number>;
  viewInvestigation: (
    president: number,
    investigated: number,
    membership: Membership,
  ) => Promise<void>;
  choosePresident: (
    president: number,
    eligiblePlayers: number[],
  ) => Promise<number>;
  playPolicy: (
    president: number,
    chancellor: number,
    hand: Membership[],
  ) => Promise<number>;
  topDeck: (played: Membership, leftInDraw: number, shuffled: boolean) => Promise<void>;
  hitlerElected: (hitler: number) => Promise<void>;
  hitlerShot: (hitler: number) => Promise<void>;
  notHitler: (notHitler: number) => Promise<void>;
  liberalPolicies: () => Promise<void>;
  fascistPolicies: () => Promise<void>;
  cardPlayed: (
    played: Membership,
    fascistPolicies: number,
    liberalPolicies: number,
    president: number,
    chancellor: number,
    leftInDraw: number,
    shuffled: boolean,
  ) => Promise<void>;
  playerShot: (victim: number) => Promise<void>;
  roundStart: (president: number) => Promise<void>;
  endGame: (roles: Role[]) => Promise<void>;
}

export const playSecretHitler = async (
  playerCount: number,
  {
    assignRoles,
    vote,
    selectChancellor,
    shoot,
    peekDeck,
    investigate,
    viewInvestigation,
    choosePresident,
    playPolicy,
    topDeck,
    hitlerElected,
    hitlerShot,
    notHitler,
    liberalPolicies,
    fascistPolicies,
    cardPlayed,
    playerShot,
    roundStart,
    endGame,
  }: SecretHitlerCallbacks,
) => {
  if (playerCount < 5) {
    throw new Error(
      `${playerCount} is too few players, need between 5 and 10 for a game of Secret Hitler.`,
    );
  }

  if (playerCount > 10) {
    throw new Error(
      `${playerCount} is too many players, need between 5 and 10 for a game of Secret Hitler.`,
    );
  }

  const getNextPresident = () => {
    if (presidentOverride !== null) {
      const next = presidentOverride;
      presidentOverride = null;
      return next;
    }
    let next = president;
    do {
      next = (next + 1) % playerCount;
    } while (killedPlayers.includes(next));
    return next;
  };

  const getLivePlayers = () =>
    [...Array(playerCount)]
      .map((_, i) => i)
      .filter(x => !killedPlayers.includes(x));

  const shuffleDeckIfNeeded = () => {
    if (draw.length < 3) {
      draw = shuffle([...draw, ...discard]);
      discard = [];
      return true;
    }
    return false;
  };

  const presidentialPowers = getPresidentialPowers(playerCount);
  const roles = getRoles(playerCount);
  await assignRoles(roles);
  let draw = getDeck();
  let killedPlayers: number[] = [];
  let discard: Membership[] = [];

  let president = 0;
  let presidentOverride: number | null = null;
  let failedVoteCount = 0;
  let passedLiberal = 0;
  let passedFascist = 0;
  let previousPresident: number | null = null;
  let previousChancellor: number | null = null;

  while (true) {
    await roundStart(president);
    let votePassed = false;
    let chancellor: number;
    const livePlayers = getLivePlayers();

    while (!votePassed) {
      chancellor = await selectChancellor(
        president,
        livePlayers.filter(
          x =>
            x !== president &&
            x !== previousChancellor &&
            (livePlayers.length <= 5 || x !== previousPresident),
        ),
      );

      votePassed = await vote(president, chancellor, livePlayers);
      if (!votePassed) {
        failedVoteCount++;
        president = getNextPresident();
        if (failedVoteCount >= 3) {
          failedVoteCount = 0;
          const played = draw.pop();
          if (!played) {
            throw Error('Tried to top deck from empty deck!');
          }
          const shuffled = shuffleDeckIfNeeded();
          previousChancellor = null;
          previousPresident = null;
          await topDeck(played, draw.length, shuffled);
          if (played === 'fascist') {
            passedFascist++;
            if (passedFascist >= 6) {
              await fascistPolicies();
              await endGame(roles);
              return;
            }
          }
          if (played === 'liberal') {
            passedLiberal++;
            if (passedLiberal >= 5) {
              await liberalPolicies();
              await endGame(roles);
              return;
            }
          }
        }
      }
      if (votePassed && passedFascist >= 3) {
        if (roles[chancellor] === 'hitler') {
          await hitlerElected(chancellor);
          await endGame(roles);
          return;
        } else {
          await notHitler(chancellor);
        }
      }

      if (votePassed) {
        previousChancellor = chancellor;
        previousPresident = president;
      }
    }

    const hand = draw.splice(-3, 3);

    const playedIndex = await playPolicy(president, chancellor!, hand);
    const played = hand[playedIndex];
    failedVoteCount = 0;
    const discarded = hand.filter((_, i) => i !== playedIndex);

    discard = [...discard, ...discarded];
    const shuffled = shuffleDeckIfNeeded();

    if (played === 'fascist') {
      passedFascist++;
      if (passedFascist >= 6) {
        await fascistPolicies();
        await endGame(roles);
        return;
      }
    }
    if (played === 'liberal') {
      passedLiberal++;
      if (passedLiberal >= 5) {
        await liberalPolicies();
        await endGame(roles);
        return;
      }
    }

    await cardPlayed(
      played,
      passedFascist,
      passedLiberal,
      president,
      chancellor!,
      draw.length,
      shuffled,
    );

    if (played == 'fascist') {
      const action = presidentialPowers[passedFascist - 1];
      switch (action) {
        case 'peekDeck':
          await peekDeck(president, draw.slice(-3));
          break;

        case 'investigate':
          const toInvestigate = await investigate(
            president,
            livePlayers.filter(x => x !== president),
          );
          await viewInvestigation(
            president,
            toInvestigate,
            roles[toInvestigate] === 'liberal' ? 'liberal' : 'fascist',
          );
          break;

        case 'choosePresident':
          presidentOverride = getNextPresident();
          president = await choosePresident(
            president,
            livePlayers.filter(x => x !== president),
          );
          continue;

        case 'shoot':
          const victim = await shoot(
            president,
            livePlayers.filter(x => x !== president),
          );

          if (roles[victim] === 'hitler') {
            await hitlerShot(victim);
            await endGame(roles);
            return;
          } else {
            await notHitler(victim);
          }

          killedPlayers = [...killedPlayers, victim];
          await playerShot(victim);
          break;
      }
    }

    president = getNextPresident();
  }
};
