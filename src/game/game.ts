import { zip } from 'lodash';
import { shuffle, delay } from '../utils';
import { playSecretHitler, Membership, Role } from './secretHitler';
import {
  sendEphemeral,
  sendMessage,
  getResponse,
  buildAssignRoleMessage,
  buildVoteMessage,
  buildVoteUpdate,
  buildChooseChancellorMessage,
  buildChooseChancellorUpdate,
  buildDrawMessage,
  buildDrawUpdate,
  buildBoardMessage,
  buildShootMessage,
  buildShootUpdate,
  startGame,
  buildPeekMessage,
  buildInvestigationMessage,
  buildInvestigationUpdate,
  buildInvestigationResultMessage,
  buildChoosePresidentMessage,
  buildChoosePresidentUpdate,
  buildTopDeckMessage,
  buildRoundStartMessage,
  buildHitlerElectedMessage,
  buildHitlerShotMessage,
  buildPlayerShotMessage,
  buildPoliciesEndMessage,
  buildVoteOutcomeMessage,
  buildNotHitlerMessage,
  buildGameEndMessage,
  buildInvestigationPublicMessage,
  buildVoteWaitingMessage,
  buildPresidentDiscardedMessage,
  buildPeekNotificationMessage,
  buildInvestigationStartingMessage,
} from '../messaging';
import { activeGames } from '../state';

export interface Player {
  name: string;
  userId: string;
  isBot: boolean;
}

export class Game {
  channel_id: string;
  channel_name: string;
  players: Player[];
  started: boolean;

  constructor(channel_id: string, channel_name: string) {
    this.channel_id = channel_id;
    this.channel_name = channel_name;
    this.players = [];
    this.started = false;
  }

  join(userId: string, name: string, isBot = false) {
    if (this.players.map((x) => x.userId).includes(userId)) {
      return 'ingame';
    }
    if (this.started) {
      return 'started';
    }

    this.players.push({ userId, name, isBot });
    console.log(`${name} just joined game`);
    return null;
  }

  start() {
    if (this.started) {
      return;
    }
    this.started = true;
    if (this.players.length < 5) {
      return false;
    }

    if (this.players.length > 10) {
      return false;
    }

    this.players = shuffle(this.players);

    console.log('game started');

    sendMessage(
      this.channel_id,
      startGame(this.players.map(({ name }) => name)),
    );

    playSecretHitler(this.players.length, {
      roundStart: async (presidentId) => {
        await delay(1000);
        const president = this.players[presidentId];
        console.log(`${president.name} is president`);
        sendMessage(this.channel_id, buildRoundStartMessage(president.name));
      },
      assignRoles: async (roles) => {
        await delay(500);

        const fascists = roles
          .map((role, i) => ({ role, i }))
          .filter(({ role }) => role === 'fascist')
          .map(({ i }) => this.players[i]);

        const hitler = this.players[roles.findIndex((x) => x === 'hitler')];

        this.players.forEach((player, i) => {
          if (!player.isBot) {
            sendEphemeral(
              this.channel_id,
              player.userId,
              buildAssignRoleMessage(roles[i], player, fascists, hitler),
            );
          }
        });
      },
      vote: async (president, chancellor, livePlayers) => {
        const players = this.players.filter((_, i) => livePlayers.includes(i));

        const ballotPromises = players.map((player) =>
          player.isBot
            ? Promise.resolve('ja')
            : getResponse(
                this.channel_id,
                player.userId,
                buildVoteMessage(
                  this.players[president].name,
                  this.players[chancellor].name,
                  this.players
                    .filter((_, i) => livePlayers.includes(i))
                    .map(({ name }) => name),
                ),
                buildVoteUpdate(
                  this.players[president].name,
                  this.players[chancellor].name,
                ),
              ),
        );

        let votingComplete = false;

        const playersVoted = players.map((x) => false);

        ballotPromises.forEach((ballotPromise, index) =>
          ballotPromise.then(() => (playersVoted[index] = true)),
        );

        delay(30000).then(() => {
          if (!votingComplete) {
            const playersNotVoted = playersVoted
              .map((x, i) => ({ x, i }))
              .filter(({ x }) => !x)
              .map(({ i }) => players[i]);
            sendMessage(
              this.channel_id,
              buildVoteWaitingMessage(playersNotVoted),
            );
          }
        });

        const ballots = await Promise.all(
          ballotPromises.map((promise) =>
            Promise.race([promise, delay(60000).then(() => 'timed out')]),
          ),
        );

        votingComplete = true;

        const yes = ballots.filter((x) => x === 'ja').length;
        const votePassed = yes > ballots.length / 2;

        console.log(votePassed ? 'vote passed' : 'vote failed');

        sendMessage(
          this.channel_id,
          buildVoteOutcomeMessage(
            votePassed,
            ballots.map((vote, i) => ({
              user: this.players[livePlayers[i]].name,
              vote,
            })),
          ),
        );

        return votePassed;
      },
      selectChancellor: async (presidentId, livePlayers) => {
        await delay(500);

        const president = this.players[presidentId];
        console.log(`${president.name} is choosing a chancellor`);
        if (president.isBot) {
          return livePlayers[0];
        }
        const choice = await getResponse(
          this.channel_id,
          president.userId,
          buildChooseChancellorMessage(
            livePlayers.map((x) => this.players[x]),
            this.players.map(({ name }) => name),
          ),
          buildChooseChancellorUpdate,
        );

        console.log('chose');

        return this.players.findIndex((x) => x.name === choice);
      },
      playPolicy: async (presidentId, chancellorId, hand) => {
        const president = this.players[presidentId];
        const chancellor = this.players[chancellorId];
        console.log('playing a policy');
        const presidentDiscard = president.isBot
          ? 0
          : parseInt(
              await getResponse(
                this.channel_id,
                president.userId,
                buildDrawMessage(hand, false),
                buildDrawUpdate(hand),
              ),
            );

        sendMessage(
          this.channel_id,
          buildPresidentDiscardedMessage(president.name, chancellor.name),
        );

        console.log(`${president.name} discarded`);

        const chancellorHand = hand.filter((_, i) => i !== presidentDiscard);

        const chancellorDiscard = chancellor.isBot
          ? 0
          : parseInt(
              await getResponse(
                this.channel_id,
                chancellor.userId,
                buildDrawMessage(chancellorHand, false),
                buildDrawUpdate(chancellorHand),
              ),
            );

        console.log(`${chancellor.name} discarded`);

        let played = 0;
        if (chancellorDiscard <= played) played++;
        if (presidentDiscard <= played) played++;

        return played;
      },
      cardPlayed: async (
        played,
        passedFascist,
        passedLiberal,
        presidentId,
        chancellorId,
        leftInDraw,
        shuffled,
      ) => {
        console.log('card played');

        const president = this.players[presidentId];
        const chancellor = this.players[chancellorId];

        sendMessage(
          this.channel_id,
          buildBoardMessage(
            played,
            this.players.length,
            passedLiberal,
            passedFascist,
            president.name,
            chancellor.name,
            leftInDraw,
            shuffled,
          ),
        );

        await delay(500);
      },
      shoot: async (presidentId, eligiblePlayers) => {
        const president = this.players[presidentId];
        console.log(`${president.name} is shooting someone`);
        if (president.isBot) {
          return eligiblePlayers[0];
        }

        const choice = await getResponse(
          this.channel_id,
          president.userId,
          buildShootMessage(eligiblePlayers.map((x) => this.players[x])),
          buildShootUpdate,
        );

        console.log(`${president.name} chose`);

        return this.players.findIndex((x) => x.userId === choice);
      },
      peekDeck: async (presidentId, top) => {
        console.log('peek power');
        const president = this.players[presidentId];

        sendMessage(
          this.channel_id,
          buildPeekNotificationMessage(president.name),
        );

        await delay(500);

        if (!president.isBot) {
          sendEphemeral(
            this.channel_id,
            president.userId,
            buildPeekMessage(top),
          );
        }
      },
      investigate: async (presidentId, eligiblePlayers) => {
        console.log('investigate power');
        const president = this.players[presidentId];
        if (president.isBot) {
          return eligiblePlayers[0];
        }

        sendMessage(
          this.channel_id,
          buildInvestigationStartingMessage(president.name),
        );

        const choice = await getResponse(
          this.channel_id,
          president.userId,
          buildInvestigationMessage(
            eligiblePlayers.map((x) => this.players[x]),
          ),
          buildInvestigationUpdate,
        );

        console.log('investigate power chosen');

        return this.players.findIndex((x) => x.userId === choice);
      },
      viewInvestigation: async (
        presidentId,
        investigatedId,
        membership: Membership,
      ) => {
        const president = this.players[presidentId];
        const investigated = this.players[investigatedId];

        sendMessage(
          this.channel_id,
          buildInvestigationPublicMessage(president.name, investigated.name),
        );

        await delay(500);

        if (!president.isBot) {
          sendEphemeral(
            this.channel_id,
            president.userId,
            buildInvestigationResultMessage(investigated.name, membership),
          );
        }
      },
      choosePresident: async (presidentId, eligiblePlayers) => {
        const president = this.players[presidentId];
        console.log(`president power for ${presidentId}`);
        if (president.isBot) {
          return eligiblePlayers[0];
        }

        const choice = await getResponse(
          this.channel_id,
          president.userId,
          buildChoosePresidentMessage(
            eligiblePlayers.map((x) => this.players[x]),
            this.players.map(({ name }) => name),
          ),
          buildChoosePresidentUpdate,
        );

        console.log('president chosen');

        return this.players.findIndex((x) => x.userId === choice);
      },
      topDeck: async (played, leftInDraw, shuffled) => {
        console.log('TOP DECK');
        sendMessage(
          this.channel_id,
          buildTopDeckMessage(played, leftInDraw, shuffled),
        );
      },
      hitlerElected: async (hitlerId) => {
        console.log('hitler elected');
        const hitler = this.players[hitlerId];
        sendMessage(this.channel_id, buildHitlerElectedMessage(hitler.name));
      },
      hitlerShot: async (hitlerId) => {
        console.log('hitler shot');
        const hitler = this.players[hitlerId];
        sendMessage(this.channel_id, buildHitlerShotMessage(hitler.name));
      },
      notHitler: async (notHitlerId) => {
        const notHitler = this.players[notHitlerId];
        sendMessage(this.channel_id, buildNotHitlerMessage(notHitler.name));
      },
      liberalPolicies: async () => {
        console.log('liberal policies');
        sendMessage(this.channel_id, buildPoliciesEndMessage('liberal'));
      },
      fascistPolicies: async () => {
        console.log('fascist policies');
        sendMessage(this.channel_id, buildPoliciesEndMessage('fascist'));
      },
      playerShot: async (victimId) => {
        console.log('player shot');
        const victim = this.players[victimId];
        sendMessage(this.channel_id, buildPlayerShotMessage(victim.name));
      },
      endGame: async (roles) => {
        console.log('end game!!!');
        const fascists = roles
          .map((role, i) => ({ role, i }))
          .filter(({ role }) => role === 'fascist')
          .map(({ i }) => this.players[i].name);

        const liberals = roles
          .map((role, i) => ({ role, i }))
          .filter(({ role }) => role === 'liberal')
          .map(({ i }) => this.players[i].name);

        const hitler = this.players[roles.findIndex((x) => x === 'hitler')]
          .name;

        await delay(500);
        sendMessage(
          this.channel_id,
          buildGameEndMessage(liberals, fascists, hitler),
        );

        this.endGame();
      },
    });
  }

  endGame() {
    activeGames.delete(this.channel_id);
  }
}
