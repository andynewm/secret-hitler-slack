import request from 'request';

export interface SlashResponse {
  token: string;
  team_id: string;
  team_domain: string;
  channel_id: string;
  channel_name: string;
  user_id: string;
  user_name: string;
  command: string;
  text: string;
  response_url: string;
  trigger_id: string;
}

export interface ActionResponse {
  user: { id: string; name: string };
  channel: { id: string };
  response_url: string;
  action_id: string;
  value: string;
  selected_option: { value: string };
}

export const getName = (user: string) =>
  new Promise<string>((resolve, reject) => {
    request(
      {
        url: 'https://slack.com/api/users.info',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          Authorization: `Bearer ${process.env.SLACK_TOKEN}`,
        },
        qs: {
          user,
        },
      },
      (error, response, body) => {
        if (error) {
          reject(error);
        }
        resolve(JSON.parse(body).user.real_name);
      },
    );
  });
